import firebase from 'firebase';
const state = {
    scores: []
}

const getters = {
    allScores: (state: any) : any => state.scores
}

const actions = {

    async fetchScores({ commit }:any){  
        var db = firebase.firestore();
        // We use  Firebase to get all the scores
        db.collection('scores').get().then((querySnapshot: any) =>{
            let allMessages:any[]=[];
            querySnapshot.forEach((doc:any) => {
                allMessages.push(doc.data())
            });

            // We order the data from the highest score to the lowest
            allMessages.sort(function (a, b) {
                return b.score - a.score;
                })
            // We only send the first 20 best scores
            commit('setScores', allMessages.slice(0,20))

        })
        
    },
    async sendScore({ commit }:any, infoPlayer: any){    
        var db = firebase.firestore();
        // We send the data to Firebase
        db.collection("scores").add({
            userName: infoPlayer.nickname,
            score: infoPlayer.score,
            // We format the time value
            date: new Date()
                .toISOString()
                .slice(2, 19)
                .replace("T", "--")
        });
    }
}
const mutations = {
    setScores: (state:any, scores:any) => (state.scores = scores)
}

export default {
    state,
    getters,
    actions,
    mutations
}