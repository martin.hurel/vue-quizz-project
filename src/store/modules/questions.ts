import axios from 'axios'

const state = {
    questions: []
}

const getters = {
    allQuestions: (state:any) : Object => state.questions
}

const actions = {
    async fetchQuestions({ commit }:any, infoPlayer: any){
        console.log(infoPlayer)
        let answers =[]
        // We fetch the data
        const response = await axios.get(
            // OpentDB is a quizz api that send question and answer depend of the paramater
            'https://opentdb.com/api.php?amount=5&category='+ infoPlayer.category+'&difficulty='+ infoPlayer.difficulty+'&type=multiple'
            )
            // The data that we receive was separte, we assemble all the answer in one array
            for(let i=0; i < response.data.results.length; i++){
                response.data.results[i].incorrect_answers.splice(Math.floor(Math.random() * 4), 0,response.data.results[i].correct_answer )            
            }
            commit('setQuestions', response.data.results)
    }
}
const mutations = {
    setQuestions: (state: any, questions: any) => (state.questions = questions)
}

export default {
    state,
    getters,
    actions,
    mutations
}