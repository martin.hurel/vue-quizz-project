import Vuex from 'vuex';
import Vue from 'vue';
import questions from './modules/questions';
import scores from './modules/scores';

// Load Vuex
Vue.use(Vuex);

// Create store
export default new Vuex.Store({
    modules:{
        questions,
        scores
    }
})