import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import firebase from 'firebase';

// Required for side-effects
require("firebase/firestore");

var firebaseConfig = {
  apiKey: "AIzaSyBf-osohFRszt8KEnSbbBHOQX50B0BcD-I",
  authDomain: "vue-quizz-app.firebaseapp.com",
  databaseURL: "https://vue-quizz-app.firebaseio.com",
  projectId: "vue-quizz-app",
  storageBucket: "vue-quizz-app.appspot.com",
  messagingSenderId: "815596645341",
  appId: "1:815596645341:web:29be3fa0f0c5e5a5142434"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
var db = firebase.firestore();
db.settings({
  timestampsInSnapshots: true
})

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
