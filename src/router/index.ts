import Vue from 'vue';
import Router from 'vue-router';
import Home from '../views/Home.vue';
import Form from '../views/Form.vue';
import Quizz from '../views/Quizz.vue';
import Scores from '../views/Scores.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/form',
      name: 'form',
      component: Form,
    },
    {
      path: '/quizz',
      name: 'quizz',
      component: Quizz,
    },
    {
      path: '/scores',
      name: 'scores',
      component: Scores,
    },
  ],
});
