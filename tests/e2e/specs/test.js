// https://docs.cypress.io/api/introduction/api.html

describe('Start a game', () => {
  it('Verify the title', () => {
    cy.visit('/')
    cy.contains('h2', 'Will you be up to it?')
  })
    it('Verify the button play go to the form', () => {
    cy.get('.play_button').click()
    cy.url().should('include', '/form')
  })
    it('Should enter a nickname', () => {
    	cy.get('#nickname').type('Test1')
    })
	it('Should select a category', () => {
    	cy.get('#category').select('9')
    })
    it('Should select a difficulty', () => {
    	cy.get('#difficulty').select('hard')
    })
    it('Should start the game', () => {
    	cy.get('.parameter__section_button.button__home').click()
    	cy.url().should('include', '/quizz')
    })

})

describe('See the top score', () => {
    it('Verify the button top score go to the page', () => {
	cy.visit('/')
	cy.get('.top_score_button').click()
	cy.url().should('include', '/scores')
  })
   
})
