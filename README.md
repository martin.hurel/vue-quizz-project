# **VUE QUIZZ**

![Repo List](public/img/logo.png)

**Welcome to the Quizz view app.**

Quizz is a quizz app that allows you to choose between different categories and difficulty level.
The goal is to have the highest score.
You have 20 seconds by question.
The faster you answser, the higher score you get.

At the end of the game your score is saved in a Firebase Database. 

You can see the top 20 players in the high score page.

***********

Quizz use Open Triviza DB API ([https://opentdb.com/](https://opentdb.com/)) to get all the questions 
and Firebase ([https://firebase.google.com/](https://firebase.google.com/))to save the player's score.

***********
To be improved:
    - the api sends text without special caracters such as ('," etc...)
    - TypeScript has some variables he's not happy with
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
